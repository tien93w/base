<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon;

class CreateSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       // create new sitemap object
    $sitemap = \App::make("sitemap");

    // add items to the sitemap (url, date, priority, freq)
    $sitemap->add(\URL::to('/'), '2012-08-25T20:10:00+02:00', '1.0', 'daily');

    // add bài viết
    $posts = DB::table('posts')->orderBy('created_at', 'desc')->get();
    foreach ($posts as $post) {
        //$sitemap->add(url, thời gian, độ ưu tiên, thời gian quay lại)
        $sitemap->add(route('allslug', $post->slug->slug), $post->updated_at, 1, 'daily');
    }

    // lưu file và phân quyền
    $sitemap->store('xml', 'sitemap');
    if (\File::exists(public_path('sitemap.xml'))) {
        chmod(public_path('sitemap.xml'), 0777);
    }
    }
}
