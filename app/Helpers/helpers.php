<?php
function categoryParent($datas ,$parent = 0, $str="", $select =0){
    foreach ($datas as $data) {
        $id = $data['id'];
        $title = $data['title'];
        if ($data['parent_id'] == $parent) {
            if ($select != 0 && $id == $select) {
                echo "<option value='$id' selected='selected' > $str $title </option>";
            }else {
                echo "<option value='$id'> $str $title </option>";
            }
            categoryParent($datas ,$id, $str."--", $select);
        }
    }

}

function menuselect($name = "menu", $menulist = array())
    {
        $html = '<select name="' . $name . '">';

        foreach ($menulist as $key => $val) {
            $active = '';
            if (request()->input('menu') == $key) {
                $active = 'selected="selected"';
            }
            $html .= '<option ' . $active . ' value="' . $key . '">' . $val . '</option>';
        }
        $html .= '</select>';
        return $html;
    }
    function showMenus($data, $parent_id = 0, $char = 'dropdown-menu', $drop = 'dropdown-toggle', $togel = 'dropdown')
    {
          $pa =[];
          foreach ($data as $key => $value) {
            array_push($pa,$value['parent']);
          }
          foreach ($data as $item)
          {
              if ($item['parent'] == $parent_id && in_array($item['id'],$pa) ) {
                if ($item['depth']>=1) {
                  echo '<li   class="'.$drop.'">';
                }else {
                  echo '<li   class="">';
                }
                echo '<a href="' . $item['link'] . '" class="'.$drop.'" data-toggle="'.$togel.'">' . $item['label'] . '</a>';
                echo '<ul  class="'.$char.'">';
                showMenus($data, $item['id'], $char='menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children', $drop='dropdown-toggle', $togel='dropdown');
                echo '</ul>';
                echo '</li>';
              }else if($item['parent'] == $parent_id) {
                echo '<li>';
                echo '<a href="' . $item['link'] . '">' . $item['label'] . '</a>';
                echo '</li>';
              }
    
          }
    
    }
    
function html_cut($text, $max_length)
{
    $tags   = array();
    $result = "";

    $is_open   = false;
    $grab_open = false;
    $is_close  = false;
    $in_double_quotes = false;
    $in_single_quotes = false;
    $tag = "";

    $i = 0;
    $stripped = 0;

    $stripped_text = strip_tags($text);

    while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
    {
        $symbol  = $text{$i};
        $result .= $symbol;

        switch ($symbol)
        {
           case '<':
                $is_open   = true;
                $grab_open = true;
                break;

           case '"':
               if ($in_double_quotes)
                   $in_double_quotes = false;
               else
                   $in_double_quotes = true;

            break;

            case "'":
              if ($in_single_quotes)
                  $in_single_quotes = false;
              else
                  $in_single_quotes = true;

            break;

            case '/':
                if ($is_open && !$in_double_quotes && !$in_single_quotes)
                {
                    $is_close  = true;
                    $is_open   = false;
                    $grab_open = false;
                }

                break;

            case ' ':
                if ($is_open)
                    $grab_open = false;
                else
                    $stripped++;

                break;

            case '>':
                if ($is_open)
                {
                    $is_open   = false;
                    $grab_open = false;
                    array_push($tags, $tag);
                    $tag = "";
                }
                else if ($is_close)
                {
                    $is_close = false;
                    array_pop($tags);
                    $tag = "";
                }

                break;

            default:
                if ($grab_open || $is_close)
                    $tag .= $symbol;

                if (!$is_open && !$is_close)
                    $stripped++;
        }

        $i++;
    }
    $ctag = count($tags);

   $ctag = count($tags);
   
    $index =1;
    while ($tags)
        switch($index)
        {
            case count($tags):
            $result .= "...</".array_pop($tags).">";
            break;
            default:
                $result .= "</".array_pop($tags).">";
        }
        $index++;

    return $result;
}