<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slug extends Model
{
    protected $fillable = [
        'slug', 'type','id'
    ];

    public function cate(){
        return $this->hasOne('App\Models\Slug','id','slug_id');
    }
}
