<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPost extends Model
{
    protected $fillable = [
        'title', 'slug_id', 'description','parent_id','image','created_at','updated_at','id'
    ];

    public function slugs()
    {
        return $this->hasOne('App\Models\Slug','id','slug_id');
    }

    public function parent(){

        return $this->belongsTo('App\Models\CategoryPost','pid');

    }
    public function posts()
    {
        return $this->belongsToMany('App\Models\Post','post_cates','post_id','cate_id');
    }
}
