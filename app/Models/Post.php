<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['id','content','slug_id','title','image','cid','uid','description','body','feature','public','viewcount','meta_title','keywords','mdescription','link1','link2','created_at','updated_at'];
    public function slugs()
    {
        return $this->hasOne('App\Models\Slug','id','slug_id');
    }

    public function users()
    {
        return $this->hasOne('App\Models\User','id','uid');
    }
    public function tag()
    {
        return $this->belongsToMany('App\Models\Tag', 'post_tag');
    }
    public function cates()
    {
        return $this->belongsToMany('App\Models\CategoryPost','post_cates','post_id','cate_id');
    }
    public function catesview()
    {
        return $this->belongsToMany('App\Models\CategoryPost','post_cates','post_id','cate_id')->take(1);
    }
    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'post_id')->where('parent_id', 0)->where('is_approved',1);
    }
}
