<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reg extends Model
{
    protected $fillable = [
        'student','date','address','addmission','parent_name','parent_phone','phone', 'facebook'
    ];
}
