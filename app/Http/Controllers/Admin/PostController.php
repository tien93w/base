<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Slug\SlugRepositoryInterface;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Tag\TagRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DataTables;

class PostController extends Controller
{
    protected $repoPost;
    protected $repoSlug;
    protected $repoCate;
    protected $repoTag;
    public function __construct(TagRepositoryInterface $repoTag,PostRepositoryInterface $repoPost,SlugRepositoryInterface $repoSlug,CategoryRepositoryInterface $repoCate)
    {
        $this->repoPost = $repoPost;
        $this->repoSlug = $repoSlug;
        $this->repoCate = $repoCate;
        $this->repoTag = $repoTag;
        $this->middleware('permission:post-list|post-create|post-edit|post-delete', ['only' => ['index','store']]);
        $this->middleware('permission:post-create', ['only' => ['create','store']]);
        $this->middleware('permission:post-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:post-delete', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repoPost->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a  href="'.route('post.edit', $row->id).'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                    <i class="la la-edit"></i></a> <a  href="'.route('allslug', $row->slugs->slug).'" target="_blank" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="View" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                    <i class="la la-eye"></i></a><a  href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser" title="View">
                    <i class="la la-close"></i></a>';

                    return $btn;
                })
                ->rawColumns(['action'])

                ->make(true);
        }
        return view('admin.post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags  = $this->repoTag->pluck('name','id');
        $data= $this->repoCate->all();
        return view('admin.post.create',compact('data','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:slugs',
            'description' => 'required',
            'image'=>'required',
            'content'=>'required',
            'cid' => 'required',

        ]);
        $inputs = $request->except(['_token','slug','type']);
        $idSlugs = $this->repoSlug->insertGetId($request->only(['slug','type']));
        $inputs['slug_id'] = $idSlugs;
        $inputs['uid'] = Auth::id();
        $data = $this->repoPost->create($inputs);
        $data->tag()->sync($request->input('tag'));
        if($data->wasRecentlyCreated === false){
            $this->repoSlug->delete($idSlugs);
        }else{
            return redirect()->route('post.index')
            ->with('success','Tạo mới bài viết thành công');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data2 = $this->repoPost->find($id);
        $tags = $this->repoTag->pluck('name', 'id');
        $data = $this->repoCate->all();
        return view('admin.post.edit',compact('data2','data','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //dd($request->contents);
        $data = $this->repoPost->find($id);
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required|unique:slugs,slug,'.$data->slug_id ,
            'description' => 'required',
            'image'=>'required',
            'content'=>'required',
            'cid' => 'required',
            'tag.*' => 'exists:tags,id',
        ]);
        $this->repoPost->update($id,$request->except('slug','_token','_method','tag','type'));
        $this->repoSlug->update($data->slug_id,['slug'=>$request->slug]);
        $data->tag()->sync((array)$request->input('tag'));
        return redirect()->route('post.index')
            ->with('success','Sửa bài viết thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repoPost->deleteWithSlug($id);
        return redirect()->route('post.index')
            ->with('success','Xóa bài viết thành công');
    }
}
