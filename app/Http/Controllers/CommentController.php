<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Post;
use App\Http\Requests\CommentUserRequest;
use App\Repositories\Comment\CommentRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Mail;

class CommentController extends Controller
{
    protected $repoComment;
    public function __construct(CommentRepositoryInterface $repoComment)
    {
        $this->repoComment = $repoComment;
    }

    public function store(CommentUserRequest $request)
    {
        $data = new Comment();
        $data->post_id = $request->post_id;
        $data->name = $request->name;
        $data->parent_id = 0;
        if (Auth::check()) {
            $data->user_id = Auth::id();
            $data->is_approved =1;
        }
        $data->email = $request->email;
        $data->body = $request->body;
        $data->phone = $request->phone;
        $data->save();
        $post = Post::findOrFail($request->post_id);
        $data['title'] = $request->name;
        $data['phone'] = $request->phone;
        $data['admission'] = $request->body;
        $data['links'] = route('allslug', $post->slugs->slug);
        $mail = Mail::send('emails.comment', $data, function($message) {

            $message->to('tien2vv@gmail.com', 'Receiver Name')->cc(['diepviencongnghe@gmail.com','truongcaodangnauan@gmail.com'])

                    ->subject('Trường cao đẳng nấu ăn Hà Nội');
        });

        return redirect()->back();
    }

    public function replyStore(CommentUserRequest $request)
    {
        $reply = new Comment();
        $reply->post_id = $request->post_id;
        $reply->name = $request->name;
        $reply->email = $request->email;
        if (Auth::check()) {
            $reply->user_id = Auth::id();
            $reply->is_approved =1;
        }
        $reply->body = $request->body;
        $reply->phone = $request->phone;
        $reply->parent_id = $request->comment_id;

        $reply->save();
        $post = Post::findOrFail($request->post_id);
        $data['title'] = $request->name;
        $data['phone'] = $request->phone;
        $data['admission'] = $request->body;
        $data['links'] = route('allslug', $post->slugs->slug);
        $mail = Mail::send('emails.comment', $data, function($message) {

            $message->to('tien2vv@gmail.com', 'Receiver Name')->cc(['diepviencongnghe@gmail.com','truongcaodangnauan@gmail.com'])

                    ->subject('Trường cao đẳng nấu ăn Hà Nội');
        });

        return  redirect()->back();

    }
}
