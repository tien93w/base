<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Http\Resources\Posts as PostsCollection;
use App\Models\CategoryPost;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\ContactMobileRequest;
use App\Models\Slug;
use Mail;
use Illuminate\Support\Str;
use App\Models\Page;
use App\Models\Slide;
use App\Models\Tag;
use App\Models\Reg;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Slug\SlugRepositoryInterface;
use App\Repositories\Page\PageRepositoryInterface;
use App\Repositories\Post\PostRepositoryInterface;
use App\Models\Contact;
//use Corcel\Model\Post;
use Illuminate\Support\Facades\Auth;
use Corcel\Model\Taxonomy;
use App\Http\Requests\RegRequest;

class HomeController extends Controller
{

  protected $cateRepository;
  protected $repoSlug;
  protected $repoPage;
  protected $repoPost;
  public function __construct(
  CategoryRepositoryInterface $cateRepository,
  SlugRepositoryInterface $repoSlug,
  PageRepositoryInterface $repoPage,
  PostRepositoryInterface $repoPost
  )
  {
      $this->cateRepository = $cateRepository;
      $this->repoSlug = $repoSlug;
      $this->repoPage = $repoPage;
      $this->repoPost = $repoPost;
  }
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$data = Post::published()->where('post_type','post')->limit(100)->get();
        $cat = $this->repoPost->limit(13)->orderBy('id','DESC')->get();
        // foreach($cat as $item){
        //   $idSlug = $this->repoSlug->insertGetId(['slug'=>$item->post_name,'type'=>'post']);
        //   $post = Post::find($item->ID);
        //   $data = $this->repoPost->create([
        //     'title'=>$item->post_title,
        //     'slug_id'=>$idSlug,
        //     'cid'=>1,
        //     'description'=>$item->meta->_genesis_description,
        //     'mdescription'=>$item->meta->_genesis_description,
        //     'meta_title'=>$item->meta->_genesis_title,
        //     'keywords'=>$item->meta->_genesis_keywords,
        //     'image'=>$post->getImageAttribute(),
        //     'content'=>$item->post_content,
        //     'uid'=>1,
        //   ]);
        //   $tagIds = [];
        //   foreach($post->tags() as $tagName)
        //   {
        //       //$post->tags()->create(['name'=>$tagName]);
        //       //Or to take care of avoiding duplication of Tag
        //       //you could substitute the above line as
        //       $tag = Tag::firstOrCreate([
        //         'name'=>$tagName,
        //         'slug'=>Str::slug($tagName,'-')
        //         ]);
        //       if($tag)
        //       {
        //         $tagIds[] = $tag->id;
        //       }

        //   }
        //   $data->tag()->sync($tagIds);
        //   $cateIds = [];
        //   foreach($post->cates() as $cate)
        //   {
        //       //$post->tags()->create(['name'=>$tagName]);
        //       //Or to take care of avoiding duplication of Tag
        //       //you could substitute the above line as
        //       $tag = CategoryPost::firstOrCreate([
        //         'title'=>$cate['name'],
        //         //'slug'=>$cate['slug']
        //         ]);
        //       if($tag)
        //       {
        //         $cateIds[] = $tag->id;
        //       }

        //   }
        //   $data->cates()->sync($cateIds);
        //   }

        // echo '<pre>';
        // print_r($cat);
        // echo '</pre';
        //$post = Post::find(2998);
        $tinlienthong = Post::whereHas('cates', function ($query){
          $query->where('cate_id',29);
        })->with('slugs')->orderBy('id', 'desc')->limit(3)->get();
        $diemchuandh = Post::whereHas('cates', function ($query){
          $query->where('cate_id',24);
        })->with('slugs')->orderBy('id', 'desc')->limit(3)->get();
        $data = [
          'postnews'=>$cat,
          'tinlienthong'=>$tinlienthong,
          'diemchuandh'=>$diemchuandh
        ];
        return view('home',$data);
    }

    public function allslug($slug){
      \Shortcode::enable();
        $dataslug = Slug::select()->where('slug', $slug)->first();
        if(!$dataslug){
            return redirect()->route('404');
        }
        if($dataslug->type =="postcate"){
          $datapc = CategoryPost::select()->where('slug_id', $dataslug->id)->first();
          $datas = Post::select()->where('cid',$datapc->id)->orderBy('id', 'desc')->paginate(10);
          $datanews = Post::select()->orderBy('id', 'desc')->limit(5)->get();
          return view('fe.catepost',compact('datas','datanews','datapc'))->withShortcodes();
        }

        if($dataslug->type =="post"){
          $data = $this->repoPost->with('catesview')->where('slug_id', $dataslug->id)->first();
          $data->setRelation('comments', $data->comments()->paginate(5));
          $cateId = $data->catesview->pluck('id');
          $cate = CategoryPost::where('id',$cateId)->with('slugs')->first();
          $datanews = Post::whereHas('cates', function ($query) use ($cateId){
            $query->where('cate_id',$cateId);
          })->with('slugs','catesview')->orderBy('id', 'desc')->limit(3)->get();
          return view('fe.post',compact('data','datanews','cate'))->withShortcodes();;
        }
        if($dataslug->type =="newcustom"){
            $data = Post::select()->where('slug_id', $dataslug->id)->first();
            $data->setRelation('comments', $data->comments()->paginate(5));
            $datanews = Post::select()->orderBy('id', 'desc')->limit(5)->get();
            return view('fe.postcustom',compact('data','datanews'))->withShortcodes();;
        }
        if($dataslug->type =="landing"){
          $data = Post::select()->where('slug_id', $dataslug->id)->first();
          $data->setRelation('comments', $data->comments()->paginate(5));
          $datanews = Post::select()->orderBy('id', 'desc')->limit(5)->get();
          return view('fe.landing',compact('data','datanews'))->withShortcodes();;
        }

        if($dataslug->type =="page"){
          $data = Page::select()->where('slug_id', $dataslug->id)->first();
          $datanews = Post::select()->orderBy('id', 'desc')->limit(10)->get();
          return view('fe.page',compact('data','datanews'))->withStripShortcodes();
        }
    }
    public function tag($slug){

      $datas = Post::whereHas('tag', function($q) use ($slug)
      {
          $q->where('slug', $slug);
      })->paginate(10);
      $datapc = Tag::select()->where('slug',$slug)->first();
      return view('fe.tagpost',compact('datas','datapc'));
    }

    public function contact(ContactRequest $request ){

        $data['title'] = $request->student;
        $data['phone'] = $request->phone;
        $data['admission'] = $request->addmission;
        $mail = Mail::send('emails.contact', $data, function($message) {

          $message->to('tien2vv@gmail.com', 'Receiver Name')->cc(['diepviencongnghe@gmail.com','truongcaodangnauan@gmail.com'])

          ->subject('Trường cao đẳng nấu ăn Hà Nội');
        });

                Reg::create($request->all());
            return redirect()->back()
            ->with('success','Thông tin được gửi thành công. Chúng tôi sẽ liên hệ lại trong thời gian sớm nhất.');


    }
    public function contactm(ContactMobileRequest $request ){

      $data['title'] = $request->student;
      $data['phone'] = $request->phone;
      $data['admission'] = $request->addmission;
      $mail = Mail::send('emails.contact', $data, function($message) {

        $message->to('tien2vv@gmail.com', 'Receiver Name')->cc(['diepviencongnghe@gmail.com','truongcaodangnauan@gmail.com'])

        ->subject('Trường cao đẳng nấu ăn Hà Nội');
      });

          Reg::create($request->all());
          return redirect()->back()
          ->with('success','Thông tin được gửi thành công. Chúng tôi sẽ liên hệ lại trong thời gian sớm nhất.');


  }
    public function xettuyen(RegRequest $request){
        Reg::create($request->all());
        $data['title'] = $request->student;
        $data['phone'] = $request->phone;
        $data['admission'] = $request->addmission;
        $mail = Mail::send('emails.contact', $data, function($message) {

          $message->to('tien2vv@gmail.com', 'Receiver Name')->cc(['diepviencongnghe@gmail.com','truongcaodangnauan@gmail.com'])

          ->subject('Trường cao đẳng nấu ăn Hà Nội');
        });
        return redirect()->back()
            ->with('success','Đăng Ký Thành Công');
    }
}
