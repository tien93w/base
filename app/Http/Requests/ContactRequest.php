<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student'=>'required',
            'date'=>'required',
            'phone'=>'required|regex:/(0)[0-9]{9}/',
            'address'=>'required',
            'parent_name'=>'required',
            'parent_phone'=>'required|regex:/(0)[0-9]{9}/',
            'addmission'=>'required',
            'point'=>'required',
            'sex'=>'required'
        ];      
    }

    public function messages(){
        return [
            'student.required'=>'Vui lòng nhập tên của bạn',
            'date.required'=>'Vui lòng nhập ngày tháng năm sinh',
            'phone.required'=>'Vui lòng nhập số điện thoại của bạn',
            'phone.regex'=>'Định dạng số điện thoại không đúng vui lòng nhập lại',
            'address.required'=>'Vui lòng nhập địa chỉ',
            'sex.required'=>'Vui lòng chọn giới tính',
            'point.required'=>'Vui lòng nhập điểm tổng kết',
            'addmission.required'=>'Vui lòng chọn hệ xét tuyển',
            'parent_name.required'=>'Vui lòng nhập tên phụ huynh',
            'parent_phone.required'=>'Vui lòng nhập số điện thoại của phụ huynh',
            'parent_phone.regex'=>'Định dạng số điện thoại không đúng vui lòng nhập lại',


        ];
    }
}
