<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required|regex:/(0)[0-9]{9}/',
            'body'=>'required'
        ];
    }
    public function messages(){
        return [
            'name.required'=>'Vui lòng nhập đầy đủ họ tên',
            'email.required'=>'Vui lòng nhập email',
            'phone.required'=>'Vui lòng nhập số điện thoại',
            'phone.regex'=>'Định dạng số điện thoại không đúng vui lòng nhập lại',
            'body.required'=>'Vui lòng nhập nội dung'
        ];
        
    }
}
