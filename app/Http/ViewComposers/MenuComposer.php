<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\MenuItems;
use App\Models\Post;
use App\Models\SiteConfig;
class MenuComposer
{


/**
      * Bind data to the view.
      *
      * @param  View  $view
      * @return void
      */
public function compose(View $view)
    {
        $view
        ->with('menus', MenuItems::select()->where('menu',1)->orderBy('sort', 'asc')->get())
        ->with('menusmobile', MenuItems::select()->where('menu',2)->orderBy('sort', 'asc')->get())
        ->with('meta', SiteConfig::FindOrFail(1));
    }
}