<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Post;
use App\Models\CategoryPost;
class SideBarComposer
{

/**
      * Bind data to the view.
      *
      * @param  View  $view
      * @return void
      */
    public function compose(View $view)
    {
        $view
        ->with('tuyensinh', Post::whereHas('cates', function ($query) {
            $query->where('cate_id','=',26);
        })->orderBy('id','desc')->take(5)->get());
        $view
        ->with('lienthong', Post::whereHas('cates', function ($query) {
            $query->where('cate_id','=',5);
        })->orderBy('id','desc')->take(6)->get());
    }
}