<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/', 'HomeController@index')->name('home');
Route::group(['prefix'=>'wp-admin','middleware' => ['auth']], function() {
    Route::get('/', function () {
        return view('admin.index');
    })->name('admin');
    Route::resource('roles','Admin\RoleController');
    Route::get('site','Admin\SiteController@update')->name('site.create');
    Route::post('site','Admin\SiteController@store')->name('site.store');
    Route::resource('user','Admin\UserController');
    //newcate
    Route::resource('postcate', 'Admin\CategoryPostController');
    Route::resource('post', 'Admin\PostController');
    //Page
    Route::resource('page', 'Admin\PageController');
    //slide
    Route::resource('slide', 'Admin\SlideController');
    //slide
    Route::resource('contact', 'Admin\ContactController');
    Route::resource('reg', 'Admin\RegController');
    //slide
    Route::resource('tag', 'Admin\TagController');
    //slide
    Route::resource('comment', 'Admin\CommentController');
    Route::post('comment/{id}', 'Admin\CommentController@storeedit')->name('commentedit');
    Route::get('commentdel/{id}', 'Admin\CommentController@delcommentfront')->name('delcommentfront');

    Route::get('/menu', array('as' => 'menu', 'uses' => 'Admin\MenuController@index'));
        Route::post('/addcustommenu', array('as' => 'haddcustommenu', 'uses' => 'Admin\MenuController@addcustommenu'));
        Route::post('/deleteitemmenu', array('as' => 'hdeleteitemmenu', 'uses' => 'Admin\MenuController@deleteitemmenu'));
        Route::post('/deletemenug', array('as' => 'hdeletemenug', 'uses' => 'Admin\MenuControllerMenuController@deletemenug'));
        Route::post('/createnewmenu', array('as' => 'hcreatenewmenu', 'uses' => 'Admin\MenuController@createnewmenu'));
        Route::post('/generatemenucontrol', array('as' => 'hgeneratemenucontrol', 'uses' => 'Admin\MenuController@generatemenucontrol'));
        Route::post('/updateitem', array('as' => 'hupdateitem', 'uses' => 'Admin\MenuController@updateitem'));


});
Route::get('/lien-he', function () {
    return view('fe.lienhe');
})->name('lienhe');
Route::get('/dang-ky-xet-tuyen', function () {
    return view('fe.dangky');
})->name('dangky');
Route::get('page-not-found',function(){
   return view('fe.404'); 
})->name('404');
Route::get('tag/{slug}', 'HomeController@tag')->name('tag');
Route::post('/contact', 'HomeController@contact')->name('contact');
Route::post('/contactm', 'HomeController@contactm')->name('contactm');
Route::post('/reg', 'HomeController@xettuyen')->name('reg');
Route::post('/comment/store', 'CommentController@store')->name('comment.add');
Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');
Route::get('/{slug}', 'HomeController@allslug')->name('allslug');
