<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'post-list',
            'post-create',
            'post-edit',
            'post-delete',
            'page-list',
            'page-create',
            'page-edit',
            'page-delete',
            'postc-list',
            'postc-create',
            'postc-edit',
            'postc-delete',
            'menu'
         ];
    
         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}
