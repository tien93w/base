@section('meta')
@if( $meta)
  <title>XÉT TUYỂN TRỰC TUYẾN 2020</title>
  <meta name="robots" content="index, follow" />
  <meta name="description" content="{{ $meta->discription }}" />
  <meta property="og:image"
    content="https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png" />
  <meta name="keywords" content=" {{ $meta->keysword }}" />
@endif
@endsection
@extends('layouts.fe')
@section('home')
<p id="title">
  <a href="{{ route('home') }}"></a>
</p>
@endsection
@section('content')
<style>
  .top-dangky {
    background-color: #e06501;
  }

  .dangky-form {
    background-color: white;
    margin-top: -35px;
    border-top: 4px solid#bf3004;
    box-shadow: 1px 1px 4px 1px #a5a2a28a;
    padding-bottom: 30px;
  }

  .dangky-form h1 {
    font-size: 23pt;
    color: #2d3238;
    text-align: center;
    font-weight: 400;
  }

  .dangky-form p {
    text-align: justify;
    padding: 0px 20px;
    text-align: center;
  }

  .dangky-form span span {
    color: red;
  }

  .btn-dangky {
    background-color: #e06501;
    border: none;
  }
</style>

<div class="container-fluid top-dangky">
  <div class="container">
    <img src="{{ asset('images/dang-ky-copy.jpg') }}" alt="">
  </div>

</div>
<div class="container">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 dangky-form">

        <script>
          window.setTimeout(function () {
            $(".alert").fadeTo(500, 0).slideUp(500, function () {
              $(this).remove();
            });
          }, 5000);
        </script>
        <h1>Mẫu Phiếu Đăng Ký Xét Tuyển</h1>
        <p><strong>Yêu Cầu Thí Sinh Điền Đầy Đủ Thông Tin Chính Xác Theo Mẫu Sau:</strong></p>
        <p>(Thông tin đăng ký của thí sinh cần thật chính xác để thầy cô có thể liên hệ lại hỗ trợ xét tuyển những
          trường đánh dấu (<span style="color:red">*</span>) là phần bắt buộc để phục vụ việc xét tuyển năm 2020</p>
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('reg') }}" method="post">
              @if(count($errors) > 0)
                <div class="alert alert-danger">
                  <strong>Lỗi!</strong><br><br>
                  <ul>
                    @foreach($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              {{ csrf_field() }}
              <div class="wrap-input100 rs1-wrap-input100 validate-input">
                <span class="label-input100">Họ và tên(<span>*</span>):</span>
                <input class="input100" type="text" value="{{ old('student') }}" name="student"
                  placeholder="Câu trả lời của bạn">
                <span class="focus-input100"></span>
              </div>

              <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
                <span class="label-input100">Ngày Tháng Năm Sinh:<span>*</span></span>
                <input class="input100" value="{{ old('date') }}" type="date" name="date"
                  value="yyyy-mm-dd" id="example-datetime-local-input">
                <!--<input class="input100" type="text" value="{{ old('date') }}" name="date" placeholder="Câu trả lời của bạn">-->
                <span class="focus-input100"></span>
              </div>

              <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
                <span class="label-input100">Số điện thoại:<span>*</span></span>
                <input class="input100" type="text" value="{{ old('phone') }}" name="phone"
                  placeholder="Câu trả lời của bạn">
                <span class="focus-input100"></span>
              </div>

              <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
                <span class="label-input100">Địa chỉ thường chú:<span>*</span></span>
                <input class="input100" type="text" value="{{ old('address') }}" name="address"
                  placeholder="Câu trả lời của bạn">
                <span class="focus-input100"></span>
              </div>

              <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
                <span class="label-input100">Họ Tên Bố Hoặc Mẹ:<span>*</span></span>
                <input class="input100" type="text" value="{{ old('parent_name') }}"
                  name="parent_name" placeholder="Câu trả lời của bạn">
                <span class="focus-input100"></span>
              </div>

              <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
                <span class="label-input100">Số Điện Thoại Bố Hoặc Mẹ:<span>*</span></span>
                <input class="input100" type="text" value="{{ old('parent_phone') }}"
                  name="parent_phone" placeholder="Câu trả lời của bạn">
                <span class="focus-input100"></span>
              </div>
              <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
                <span class="label-input100">Chọn Hệ Đăng Ký:<span>*</span></span>
                <div class="radio">
                  <input id="radio-1" name="addmission" value="Hệ cao đẳng nấu ăn" type="radio" checked>
                  <label for="radio-1" value="Hệ cao đẳng nấu ăn" class="radio-label">Cao Đẳng Nấu Ăn</label>
                </div>
                <div class="radio">
                  <input id="radio-2" name="addmission" value="Hệ trung cấp nấu ăn" type="radio">
                  <label for="radio-2" value="Hệ trung cấp nấu ăn" class="radio-label">Trung Cấp Nấu Ăn</label>
                </div>
                <div class="radio">
                  <input id="radio-3" name="addmission" value="Sơ cấp nấu ăn" type="radio">
                  <label for="radio-3" value="Sơ cấp nấu ăn" class="radio-label">Sơ Cấp Nấu Ăn</label>
                </div>

              </div>


              <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
                <span class="label-input100">Địa Chỉ Facebook(Điền link dẫn đến trang cá nhân):</span>
                <input class="input100" type="text" name="facebook" placeholder="Câu trả lời của bạn">
                <span class="focus-input100"></span>
              </div>

              <button class="btn btn-success btn-dangky" type="submit">Đăng Ký</button>
            </form>
          </div>
        </div>

      </div>

    </div>
  </div>
</div>
<style>

</style>
<div class="modal fade" id="myModal" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: none;
    border-radius: inherit;text-align: center">
      <div class="modal-body">
        <div class="row">
          <i class="fa fa-check-circle 2x center-block" style="font-size: 39px;color: #00A000" aria-hidden="true"></i>
        </div>
        <div class="row" style="color: #00A000">
          <h2>Đăng Ký Thành Công</h2>
          <p>Để có thể chủ động hơn trong liên hệ với các thầy, cô. Bạn hãy like share và nhắn tin tại fanpage của nhà
            trường để được tư vấn nhiều hơn!</p>
        </div>
        <div class="btn-page-new">
          <span><a href="{{ route('home') }}" class="btn-resgister-page">Về Trang Chủ<i
                class="fa fa-long-arrow-right" aria-hidden="true"></i></a></span><span><a href="#"
              class="btn-tu-van-page">Fanpage<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></span>
        </div>
      </div>
    </div>
  </div>
</div>




@endsection
@section('js')
@if(session('success'))
  <script>
    $(function () {
      $('#myModal').modal('show');
    });
  </script>
@endif
@endsection
