@section('meta')
@if($data && $meta)
  <title>
    {{ $data->meta_title != "" ? $data->meta_title : $data->title }}
  </title>
  <meta name="robots" content="index, follow" />
  <meta name="description"
    content="{{ $data->mdescription ? $data->mdescription : $meta->discription }}" />
  <meta property="og:image"
    content="{{ $data->image != "" ? $data->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png' }}" />
  <meta name="keywords" content="{{ $data->keywords ? $data->keywords : $meta->keysword }}" />
@endif
@endsection
@extends('layouts.fe')
@section('home')
<p id="title">
  <a href="{{ route('home') }}"></a>
</p>
@endsection
@section('content')
    <!-- content -->
      <div class="container boder-solid1 p-md-2 mt-md-2">
          <div class="row">
              <div class="col-md-9 page-content">
              <h1>{{$data->title}}</h1>
              <?php //dd($data->cates->pluck('id'))?>
                  <div class="col-12 row page-ads">
                      <img src="./images/page1.jpg" class="img-fluid" alt="">
                  </div>
                  <div class="col-12 row breabcrumb">
                      <div class="col-6"><span><i class="fa fa-home" aria-hidden="true"></i>
                      </span><span><a href="">Trang chu</a></span><span style="color:#007bff"> ></span><span><a href="{{ route('allslug',$cate->slugs->slug)}}"> {{$cate->title}}</a></span>
                      </div>
                      <div class="col-6" style="    text-align: end;">
                          <span class="span-line-height-fb-btn">
                              <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/"
                                  data-width="" data-layout="button_count" data-action="like" data-size="small"
                                  data-share="true"></div>
                          </span>
                          <span><a class="twitter-share-button" target="_blank"
                                  href="https://twitter.com/intent/tweet?text=Hello%20world" data-size="large"><i
                                      class="fa fa-twitter" aria-hidden="true"></i>
                                  Tweet</a>
                          </span>
                      </div>
                  </div>
                  <div class=" row page-text">
                      <div class="d-none d-sm-block col-2 social-left">
                          <ul>
                              <li class="page-youtube"><a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-twitter"><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                              </li>
                              <li class="page-facebook"><a href=""><i class="fa fa-facebook"
                                          aria-hidden="true"></i></a></li>
                              <li class="page-comment"><a href=""><i class="fa fa-comments"
                                          aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                      <div class="col-md-10 col-xs-12 page-left">
                        {!! $data->content!!}
                      </div>
                  </div>


                  <div class="col-12 row">
                      <div class="col-6">
                          <p class="pl-md-4">BẠN THÍCH BÀI VIẾT NÀY ? <span class="pl-md-4"><i
                                      class="fa fa-long-arrow-right" aria-hidden="true"></i></span></p>
                      </div>
                      <div class="col-6" style="    text-align: end;">
                          <span class="span-line-height-fb-btn">
                              <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/"
                                  data-width="" data-layout="button_count" data-action="like" data-size="small"
                                  data-share="true"></div>
                          </span>
                          <span><a class="twitter-share-button" target="_blank"
                                  href="https://twitter.com/intent/tweet?text=Hello%20world" data-size="large"><i
                                      class="fa fa-twitter" aria-hidden="true"></i>
                                  Tweet</a>
                          </span>
                      </div>
                  </div>

                  <div class="">
                      <div class="row block-two-title">
                          <h2>TIN LIEN QUAN</h2>
                      <span class="block-two-link"><a style="border-right: none;"></a></span><span><a href="{{ route('allslug',$cate->slugs->slug)}}"
                                  class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                      aria-hidden="true"></i></a></span>
                      </div>
                      <div class="block-two-ct3">
                        @foreach ($datanews as $item)
                          <div class=" col-xs-12 col-md-4  col-sm-12">
                          <a href="{{ route('allslug',$item->slugs->slug)}}">
                          <img src="{{ $item->image}}" class="img-fluid" alt="">
                            </a>
                            <a href="" class="block-two-ct3-h2">
                                <h2>{{$item->title}}</h2>
                            </a>
                            <p>{{$item->description}}</p>
                        </div>
                        @endforeach
                          <div class="clear"></div>
                      </div>
                  </div>

              </div>
              <div class="col-md-3">
                  @include('fe.sidebar')


              </div>
          </div>
      </div>
  <!-- end content -->

<div class="modal fade" id="myModal" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: none;
     border-radius: inherit;text-align: center">
      <div class="modal-body">
        <div class="row">
          <i class="fa fa-check-circle 2x center-block" style="font-size: 39px;color: #00A000" aria-hidden="true"></i>
        </div>
        <div class="row" style="color: #00A000">
          @if($message = Session::get('success'))
            <h2>{{ $message }}</h2>
          @endif

          <p>Để có thể chủ động hơn trong liên hệ với các thầy, cô. Bạn hãy like share và nhắn tin tại fanpage của nhà
            trường để được tư vấn nhiều hơn!</p>
        </div>
        <div class="btn-page-new">
          <span><a href="{{ route('home') }}" class="btn-resgister-page">Về Trang Chủ<i
                class="fa fa-long-arrow-right" aria-hidden="true"></i></a></span><span><a href="#"
              class="btn-tu-van-page">Fanpage<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></span>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModaledit" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: none;
       border-radius: inherit;text-align: center">
      <div class="modal-body">
        <div class="row">
          <i class="fa fa-check-circle 2x center-block" style="font-size: 39px;color: #00A000" aria-hidden="true"></i>
        </div>
        <div class="row" style="color: #00A000">
          @if($message = Session::get('successedit'))
            <h2>{{ $message }}</h2>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')

<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
  src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
  nonce="2wLpfQ0N"></script>
@if(session('success'))
  <script>
    $(function () {
      $('#myModal').modal('show');
    });
  </script>
@endif
@if(session('successedit'))
  <script>
    $(function () {
      $('#myModaledit').modal('show');
    });
  </script>
@endif
@endsection
