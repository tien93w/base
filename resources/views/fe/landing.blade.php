@section('meta')
@if($data && $meta)
  <title>
    {{ $data->meta_title != "" ? $data->meta_title : $data->title }}
  </title>
  <meta name="robots" content="index, follow" />
  <meta property="og:image"
    content="{{ $data->image != "" ? $data->image: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png' }}" />

  <meta name="keywords" content="{{ $data->keywords ? $data->keywords : $meta->keysword }}" />
@endif
@endsection
@extends('layouts.fe')
@section('home')
<p id="title">
  <a href="{{ route('home') }}"></a>
</p>
@endsection
@section('content')
<style>

</style>
<div class="" id="inner">
  <div class="wrap">
    <div id="content-category-wrap " class="top-p20">
      <div id="content" class="container">
        <div class="page-content">
          <div class="col-md-12 no-padding ladding-image-thumnails">
            <img src="{{ $data->image }}" alt="{{ $data->title }}">
          </div>

          <div class="col-md-12 top-p20">
            <div class="row">
              <div class="clear" data-sticky-container></div>
              <div class="col-md-12 no-padding">
                <h1 class="categoty-title">{{ $data->title }}</h1>
              </div>
              <div class="col-md-12">
                <div class="post-content-text">
                  <div class="content-page-description">
                    <strong>{!! $data->description!!}</strong>
                  </div>

                  {!! $data->content !!}
                </div>


                <div class="row section-ulnew">
                  <div class="col-md-12 header-relate-post">
                    <h2>TIN CÙNG CHUYÊN MỤC</h2>
                  </div>
                  <div class="col-md-12">
                    <ul style="margin-left: 0.5%;list-style:none" class="post-cung-chuyenmuc">
                      @foreach($datanews as $item)
                        <li>
                          <a href="{{ route('allslug',$item->slugs->slug) }}"
                            title="{{ $item->title }}">{{ ucwords($item->title) }}</a>
                        </li>

                      @endforeach

                    </ul>
                  </div>

                </div>



                @if(count($data->tag)>0)
                  <div class="row">
                    <div class="col-md-12 tag-post">
                      <span>Từ khoá:</span>
                      @foreach($data->tag as $singleTag)

                        <span class="label label-info label-many label-tag"><a
                            href="{{ route('tag',$singleTag->slug) }}" target="_blank"
                            rel="noopener noreferrer">{{ $singleTag->name }}</a></span>
                      @endforeach
                    </div>

                  </div>
                @endif


                {{-- comment --}}
                <div class="row">
                  <div class="col-md-12">
                    <div id="comments" class="comments-area">
                      <div id="respond" style="width: 0;height: 0;clear: both;margin: 0;padding: 0;"></div>
                      <div id="wc-comment-header">
                        <h3>
                          Bình Luận Của Bạn:

                        </h3>
                        <p>Nếu bạn có thắc mắc, ý kiến đóng góp của bạn xung quanh vấn đề này. Vui lòng điền thông tin
                          theo mẫu bên dưới rồi nhấn nút GỬI BÌNH LUẬN. Mọi ý kiến đóng góp đều được nhà trường đón đợi
                          và quan tâm. Những câu hỏi sẽ được các thầy cô trả lời và giải đáp trong thời gian sớm nhất
                        </p>

                      </div>
                      <div id="wpcomm" class="wpdiscuz_auth wpd-default">
                        <div class="wc-form-wrapper wc-main-form-wrapper" id="wc-main-form-wrapper-0_0">
                          <div class="wpdiscuz-comment-message" style="display: block;"></div>
                          <form class="wc_comm_form wc_main_comm_form" method="post" enctype="multipart/form-data"
                            action="{{ route('comment.add') }}">
                            @csrf
                            <div class="wc-field-comment">
                              <div class="wpdiscuz-item wc-field-textarea">
                                <div class="wpdiscuz-textarea-wrap ">
                                  <div class="wc-field-avatararea" style="display: block;">
                                    <img alt="Giáo Viên Phụ trách"
                                      src="https://secure.gravatar.com/avatar/e5834b67a495187a3ee7893b2353e720?s=40&amp;d=mm&amp;r=g"
                                      srcset="https://secure.gravatar.com/avatar/e5834b67a495187a3ee7893b2353e720?s=80&amp;d=mm&amp;r=g 2x"
                                      class="avatar avatar-40 photo" height="40" width="40">
                                  </div>
                                  <textarea id="wc-textarea-0_0" placeholder="Tham gia thảo luận..." required=""
                                    id="body-form" data-toggle="collapse" data-target="#form-comment" name="body"
                                    class="wc_comment wpd-field"></textarea>
                                  <div class="autogrow-textarea-mirror"
                                    style="display: none; overflow-wrap: break-word; padding: 25px 78px 0px 85px; width: 595px; font-family: &quot;Helvetica Neue&quot;, Arial, Helvetica, sans-serif; font-size: 17px; line-height: 20px;">
                                    .<br>.</div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                            </div>
                            <div class="wc-form-footer" style="display: block;">
                              <div class="collapse wpd-form-row" id="form-comment">
                                <input type="hidden" value="{{ Auth::id() }}" name="user_id">
                                <input type="number" style="display:none" name="post_id" value="{{ $data->id }}" />
                                <div class="wpd-form-col-left">
                                  <div class="wpdiscuz-item wc_name-wrapper wpd-has-icon">
                                    <div class="wpd-field-icon"><i class="fa fa-user"></i></div>
                                    <input
                                      value="{{ Auth::check() ? Auth::user()->name: old('name') }}"
                                      required="required" class="wc_name wpd-field" type="text" name="name"
                                      placeholder="Họ Và Tên*" maxlength="50" pattern=".{3,50}" title="">
                                    <div class="wpd-field-desc"><i class="fa fa-question-circle"
                                        aria-hidden="true"></i><span>Nhập họ tên đầy đủ</span></div>
                                  </div>
                                  <div
                                    class="wpdiscuz-item custom_field_5ea2bc76c8f3f-wrapper wpd-has-icon wpd-has-desc">
                                    <div class="wpd-field-icon"><i style="opacity: 0.8;" class="fa fa-phone"></i></div>
                                    <input required="required"
                                      class="custom_field_5ea2bc76c8f3f wpd-field wpd-field-number" type="text"
                                      name="phone"
                                      value="{{ Auth::check() ? '0997654321': old('phone') }}"
                                      placeholder="Điện Thoại*">
                                    <div class="wpd-field-desc"><i class="fa fa-question-circle"
                                        aria-hidden="true"></i><span>Điện thoại liên hệ</span></div>
                                  </div>
                                  <div class="wpdiscuz-item wc_email-wrapper wpd-has-icon">
                                    <div class="wpd-field-icon"><i class="fa fa-at"></i></div>
                                    <input
                                      value="{{ Auth::check() ? Auth::user()->email: old('email') }}"
                                      required="required" class="wc_email wpd-field" type="email" name="email"
                                      placeholder="Địa chỉ Email*">
                                    <div class="wpd-field-desc"><i class="fa fa-question-circle"
                                        aria-hidden="true"></i><span>Thư điện tử</span></div>
                                  </div>
                                </div>
                                <div class="wpd-form-col-right">
                                  <div class="wc-field-submit">
                                    <label class="wpd_label" title="Stick this comment">
                                      <input id="wc_sticky_comment" class="wpd_label__checkbox" value="1"
                                        type="checkbox" name="wc_sticky_comment">
                                      <span class="wpd_label__text">
                                    </label>
                                    <input class="wc_comm_submit wc_not_clicked button alt" type="submit" name="submit"
                                      value="Gửi bình luận">
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                          </form>
                        </div>
                        <div id="wcThreadWrapper" class="wc-thread-wrapper">

                          @include('partials._comment_replies', ['comments' => $data->comments, 'post_id' => $data->id])
                          <div class="wpdiscuz-comment-pagination">
                            <div class="wc-load-more-submit-wrap">
                              <div class="wc-load-more-link" data-lastparentid="34319">
                                {{ $data->comments->links() }}
                              </div>
                            </div>
                            <input id="wpdiscuzHasMoreComments" type="hidden" value="1">
                          </div>

                        </div>
                        <div class="wpdiscuz_clear"></div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- end comment --}}
                {{-- end comment --}}
              </div>

            </div>
          </div>




        </div>
        {{-- end-md 9 --}}
        <div class="siderbar-content">
          @include('fe.sidebar')

        </div>
        <div id="positionneo"></div>
      </div>




    </div>
  </div>
</div>
<div class="modal fade" id="myModal" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: none;
       border-radius: inherit;text-align: center">
      <div class="modal-body">
        <div class="row">
          <i class="fa fa-check-circle 2x center-block" style="font-size: 39px;color: #00A000" aria-hidden="true"></i>
        </div>
        <div class="row" style="color: #00A000">
          @if($message = Session::get('success'))
            <h2>{{ $message }}</h2>
          @endif

          <p>Để có thể chủ động hơn trong liên hệ với các thầy, cô. Bạn hãy like share và nhắn tin tại fanpage của nhà
            trường để được tư vấn nhiều hơn!</p>
        </div>
        <div class="btn-page-new">
          <span><a href="{{ route('home') }}" class="btn-resgister-page">Về Trang Chủ<i
                class="fa fa-long-arrow-right" aria-hidden="true"></i></a></span><span><a href="#"
              class="btn-tu-van-page">Fanpage<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></span>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModaledit" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border: none;
         border-radius: inherit;text-align: center">
      <div class="modal-body">
        <div class="row">
          <i class="fa fa-check-circle 2x center-block" style="font-size: 39px;color: #00A000" aria-hidden="true"></i>
        </div>
        <div class="row" style="color: #00A000">
          @if($message = Session::get('successedit'))
            <h2>{{ $message }}</h2>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')

<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
  src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
  nonce="2wLpfQ0N"></script>

@if(session('success'))
  <script>
    $(function () {
      $('#myModal').modal('show');
    });
  </script>
@endif
@if(session('successedit'))
  <script>
    $(function () {
      $('#myModaledit').modal('show');
    });
  </script>
@endif
@endsection