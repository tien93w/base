@section('meta')
@if ( $meta)
<title>LIÊN HỆ</title>
<meta name="robots" content="index, follow" />
<meta name="description" content="{{$meta->discription}}"/>
<meta name="keywords" content=" {{$meta->keysword}}"/>
@endif
@endsection
@extends('layouts.fe')
@section('home')
<p id="title">
            	<a href="{{ route('home')}}"></a>
            </p>
@endsection
@section('content')

<div class="container">
    <div class="wrap">
      <div id="content-category-wrap " class="top-p20">
          <div id="content" class="row">
              <div class="col-md-12">
                  <div class="col-md-12"><h2 class="categoty-title">LIÊN HỆ</h2></div>
                  <div class="col-md-12 top-p20 bot-40">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="contact-wrapper text-center">
                            <div class="contact-title">
                              <h4>
                                Địa chỉ:
                              </h4>
                            </div>
                            <div class="contact-info">
                              73 Nguyễn Chí Thanh, P. Láng Thượng, Q. Đống Đa, TP Hà Nội
                            </div>
                          </div>

                          <div class="contact-wrapper text-center">
                            <div class="contact-title">
                              <h4>
                                số điện thoại:
                              </h4>
                            </div>
                            <div class="contact-info">
                              (024) 3200 5261 - 0979 499 131
                            </div>
                          </div>

                          <div class="contact-wrapper text-center">
                            <div class="contact-title">
                              <h4>
                                email:
                              </h4>
                            </div>
                            <div class="contact-info">
                              truongcaodangnauan@gmail.com
                            </div>
                          </div>

                        </div>

                        <div class="col-md-4">
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.223035610965!2d105.80865731457827!3d21.023759986001053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab68bb0166f7%3A0xc3c072476a2aa6de!2zNzMgTmd1eeG7hW4gQ2jDrSBUaGFuaCwgVGjDoG5oIEPDtG5nLCDEkOG7kW5nIMSQYSwgSMOgIE7hu5lpLCBWaWV0bmFt!5e0!3m2!1sen!2sus!4v1592194275241!5m2!1sen!2sus" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>

                        <div class="col-md-4">
                          <h4 style="padding-bottom: 10px;font-weight:bold">FORM LIÊN HỆ</h4>
                          <form action="">
                            <div class="form-group">
                              <input type="text" name="name" placeholder="Họ tên của bạn" class="form-control">
                            </div>
                            <div class="form-group">
                              <input type="text" name="email" placeholder="Địa chỉ email của bạn" class="form-control">
                            </div>
                            <div class="form-group">
                              <input type="text" name="phone" placeholder="Số điện thoại của bạn" class="form-control">
                            </div>
                            <div class="form-group">
                              <textarea name="content" class="form-control" id="content" cols="100%" rows="4" placeholder="Nội dung"></textarea>
                            </div>
                            <div class="form-group">
                              <button class="btn btn-success" type="submit">GỬI</button>
                            </div>
                          </form>
                        </div>
                      </div>
                  </div>
              </div>
              {{-- end-md 12 --}}
          </div>

      </div>
    </div>
</div>

@endsection
