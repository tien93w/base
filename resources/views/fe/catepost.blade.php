@section('meta')
@if($datapc && $meta)
    <title>{{ $datapc->title ? $datapc->title : "" }}</title>
    <meta name="robots" content="index, follow" />
    <meta name="description"
        content="{{ $datapc->description ? $datapc->description : $meta->discription }}" />
    <meta property="og:image"
        content="{{ $datapc->image != "" ? $data->datapc: 'https://truongcaodangnauan.edu.vn/test_disk/photos/5/banner-tuyen-sinh-cao-dang-nau-an12.png' }}" />
    <meta name="keywords"
        content="{{ $datapc->keywords ? $datapc->keywords : $meta->keysword }}" />
@endif
@endsection
@extends('layouts.fe')
@section('home')
<p id="title">
    <a href="{{ route('home') }}"></a>
</p>
@endsection
@section('content')
<div class="" id="inner">
    <div class="wrap">
        <div id="content-category-wrap " class="top-p20">
            <div id="content" class="container">
                <div class="page-content">
                    <div class="col-md-12 no-padding">
                        <h1 class="categoty-title">{{ $datapc->title }}</h1>
                    </div>
                    <div class="fb-like" data-href="{{ Request::url() }}" data-width="" data-layout="standard"
                        data-action="like" data-size="large" data-share="true"></div>
                    <div class="col-md-12 top-p20">
                        <div class="row padding-480px">
                            <div class="clear" data-sticky-container></div>
                            <div class="">
                                <div class="row ">
                                    @if(count($datas)>0)
                                        @foreach($datas as $data)
                                            <div class="widget-tuyendung">
                                                <div class="col-md-4">
                                                    <a
                                                        href="{{ route('allslug', $data->slugs->slug) }}">
                                                        <img src="{{ $data->image }}">
                                                    </a>
                                                </div>
                                                <div class="col-md-8">
                                                    <h2><a class="title-news "
                                                            href="{{ route('allslug', $data->slugs->slug) }}">{{ $data->title }}</a>
                                                    </h2>
                                                    <span class="time"></span>
                                                    <p>{!! preg_replace('/\s+?(\S+)?$/', '', substr($data->description,
                                                        0, 281))!!}...</p>
                                                    <p></p>
                                                    <span class="xt">
                                                        <a
                                                            href="{{ route('allslug', $data->slugs->slug) }}">Xem
                                                            tiếp</a>
                                                    </span>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <p>Không tìm thấy bài viết</p>
                                            </div>
                                        </div>
                                    @endif
                                    {{ $datas->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end-md 9 --}}
                <div class="siderbar-content">
                    @include('fe.sidebar')
                </div>
                <div id="positionneo"></div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
    nonce="2wLpfQ0N">
</script>
<script>


@endsection
