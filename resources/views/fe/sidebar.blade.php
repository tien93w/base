<div class="page-siderbar">
   <h2 class="page-siderbar-title">THÔNG TIN GIÁO DỤC</h2>

   <div class="row m-0 ">
      <ul class="pl-0">
         <?php //dd($tuyensinh);?>
         @foreach ($tuyensinh as $item)
            <li class="pt-1">
               <div class="col-md-3 p-0">
                  <a href="{{ route('allslug',$item->slugs->slug)}}">
                     <img src="{{ $item->image}}" class="img-fluid" alt="">
                  </a>
               </div>
               <div class="col-md-9">
                  <a href="{{ route('allslug',$item->slugs->slug)}}">
                     <h2>{{ $item->title}}</h2>
                  </a>
               </div>
            </li>
         @endforeach
      </ul>
   </div>
</div>


<div class="page-siderbar-2 pt-3">
   <h2 class="page-siderbar-title">THÔNG TIN GIÁO DỤC</h2>

   <div class="row m-0 ">
      <ul class="pl-0" style="    display: contents;">
         @foreach ($lienthong as $item)
         <li class="pt-1 col-4 p-0">
               <div class="col-md-12 p-0">
                  <a href="{{ route('allslug',$item->slugs->slug)}}">
                  <img src="{{ $item->image}}" class="img-fluid" alt="">
                  </a>
               </div>
               <div class="col-md-12 p-0">
                  <a href="{{ route('allslug',$item->slugs->slug)}}">
                  <h2>{{ $item->title}}</h2>
               </a>
               </div>

         </li>
         @endforeach

      </ul>
   </div>
</div>

<div class="page-ads-img">
   <div class="row m-0">
      <img src="./images/page-ads.jpg" class="img-fluid" alt="">
      <img src="./images/page-ads.jpg" class="img-fluid" alt="">
   </div>
</div>