@section('meta')
@if( $meta)
    <title>
        {{ $meta->title ? $meta->title : "CAO ĐẲNG NẤU ĂN HÀ  NỘI" }}
        - 404</title>
    <meta name="description" content="{{ $meta->discription }}" />
    <meta name="keywords" content=" {{ $meta->keysword }}" />
@endif
@endsection
@extends('layouts.fe')
@section('home')
<p id="title">
    <a href="{{ route('home') }}"></a>
</p>
@endsection
@section('content')

<div class="container">
    <div class="wrap">
        <div id="content-category-wrap " class="top-p20">
            <div id="content" class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <h2 class="categoty-title"> LỖI 404</h2>
                    </div>
                    <div class="col-md-12 top-p20 bot-40">
                        <div class="row">
                            <h1>Lỗi trang không tồn tại</h1>
                            <a href="{{ route('home') }}">Quay lại trang chủ</a>
                        </div>
                    </div>
                </div>
                {{-- end-md 12 --}}
            </div>

        </div>
    </div>
</div>


@endsection
