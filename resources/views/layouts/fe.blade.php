@include('layouts.fe.header')
<body>
@include('include.afterbody')
    <div class="container">
        <div class="row topbar">
            <div class="col-md-6 topbar-left">
                <ul>
                    <li><span>Connect with us:</span></li>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-md-6 topbar-right">
                <ul>
                    <li>
                        <a href=""><i class="fa fa-handshake-o" aria-hidden=""></i>Hợp tác tuyển sinh</a>
                        <a href=""><i class="fa fa-bullhorn" aria-hidden="true"></i>Liên hệ quảng cáo</a>
                        <a href=""><i class="fa fa-lock" aria-hidden="true"></i>Chính sách bảo mật</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid logo-banner">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 col-md-4  col-sm-12">
                    <a href="">
                        <img class="img-fluid" src="./images/logo.png" alt="">
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="banner-adsaaaa">
                        <img class="img-fluid" src="./images/top-banner.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid main-menu p-xs-0">
        <div class="container p-xs-0">
            <div class="">

                <nav class="navbar navbar-expand-md  navbar-light">
                    <a class="navbar-brand  navbar-home" href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="index.html">Home <span
                                        class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item-2 dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    Dropdown
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">Disabled</a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </div>
    </div>
    <!-- content -->
    <div class="wrap">
        @yield('content')
    </div>
    <!-- end content -->
@include('layouts.fe.footer')
@include('include.beforebodyend')
</body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
    src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=291434641372963&autoLogAppEvents=1"
    nonce="SSfJPm6g"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="{{ asset('js/main.js')}}"></script>
@yield('js')

</html>