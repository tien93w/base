    <!-- footer -->
    <footer>
        <div class="container-fluid nav-footer">
            <div class="container">
                <div class="row">
                    <ul>
                        <li>
                            <a href="">
                                Đại Học
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Cao Đẳng
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Trung Cấp
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Liên Thông
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Chứng Chỉ
                            </a>
                        </li>
                        <li>
                            <a href="">
                                THPT
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Điểm Chuẩn
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Lao Động – Du học
                            </a>
                        </li>
                        <li>
                            <a href="">
                                Tin Giáo dục
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="container footer-bottom-end">
                <div class="row">
                    <div class="col-md-6 footer-left-one">
                        <div class="row m-0 pt-2">
                            <div class="col-md-6">
                                <img src="./images/logo.png" class="img-fluid" alt="">
                            </div>
                            <div class="col-md-6 p-0">
                                <h3 class="footer-left-one-title">THÔNG TIN TUYỂN SINH TOÀN QUỐC</h3>
                                <p><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                    kenhtuyensinh24h.vn@gmail.com</p>
                            </div>
                        </div>
                        <p>Diễn đàn tuyển sinh 24h .com chào mừng độc giả mọi thông tin thắc mắc
                            cần hỗ trợ vui lòng liên hệ theo Email chúng tôi luôn sẵn sàng đón đợi và giải đáp </p>

                    </div>
                    <div class="col-md-6">
                        <div class="fb-page" data-href="https://www.facebook.com/FPolyPythonTrainingPro"
                            data-width="500px" data-hide-cover="false" data-show-facepile="false">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class=" col-xs-12 col-md-4  col-sm-12">
                        <h3>TRỤ SỞ HÀ NỘI</h3>
                        <p>Tầng 23, Tòa nhà Meco comlex 102 Trường Chinh
                            Quận Thanh Xuân, TP. Hà Nội
                        </p>
                        <p>
                            Tel: 024.3200 5561 - 0978 889 889
                        </p>
                        <h3>VĂN PHÒNG TUYỂN SINH</h3>
                        <p>
                            Số 73 Nguyễn chí Thanh , Phường. Láng hạ
                            Quận Đống đa, Thành Phố Hà nội
                        </p>
                        <p>
                            Tel: 024.3200 5561 - 0978 889 889
                        </p>
                    </div>
                    <div class=" col-xs-12 col-md-4  col-sm-12">
                        <h3>
                            CHỊU TRÁCH NHIỆM QUẢN LÝ NỘI DUNG
                        </h3>
                        <p>
                            Ông Lương Minh Vũ
                        </p>
                        <h3>HỢP TÁC TUYỂN SINH</h3>
                        <p>024.3200 5561 - 0978 889 889</p>
                        <p>kenhtuyensinh24h.vn@gmail.com</p>
                        <h3>LIÊN HỆ QUẢNG CÁO</h3>
                        <p>0978 889 889</p>
                        <p>samset.edu@gmail.com</p>
                        <p><a href="" class="footer-btn-chat"><i class="fa fa-commenting-o" aria-hidden="true"></i> CHAT
                                VỚI TƯ VẤN</a></p>

                        <p class="pt-2"><a href="" class="footer-btn-view">XEM CHI TIẾT</a></p>
                        <h3>CHÍNH SÁCH BẢO MẬT </h3>
                    </div>
                    <div class=" col-xs-12 col-md-4  col-sm-12">
                        <h3>VẬN HÀNH BỞI</h3>
                        <img src="./images/logo.png" class="img-fluid" alt="">
                        <p>@Copyright 2017 - 2020 - SẤM SÉT EDUCATION</p>
                        <p>Tầng 23, Tòa nhà Meco comlex 102 Trường Chinh
                            Quận Thanh Xuân, TP. Hà Nội</p>
                        <p>024.3200 5561 - 0978 889 889</p>
                        <p>samset.edu@gmail.com</p>

                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->