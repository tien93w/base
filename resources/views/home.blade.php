@extends('layouts.fe')
@section('content')
<!-- block one -->
<div class="container-fluid block-one">
    <div class="container">
        <div class="row">
            <div class="w-68">
                <div class="w-58 block-one-left">
                    <img src="./images/block-11.jpg" class="img-fluid" alt="">
                    <a href="">
                        <h2>Thi Tốt Nghiệp THPT Năm 2020 Có Nhiều Thay Đổi Do Dịch Covid 19</h2>
                    </a>
                    <div>
                        <ul>
                            <li><a href="">Hướng Dẫn Tra Điểm Thi THPT Quốc Gia 2019 Theo Tên Đơn Giản Nhất</a>
                            </li>
                            <li><a href="">Các Trường Đại Học Có Ngành Ngôn Ngữ Trung Đào Tạo Chất Lượng
                                    Nhất</a></li>
                        </ul>
                    </div>
                </div>
                <div class="w-42 block-one-center">
                    <h2>TIN TỨC TUYỂN SINH</h2>
                    <ul>
                        @foreach ($postnews as $item)
                    <li><a href="{{ route('allslug',$item->slugs->slug)}}">{{$item->title}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="w-32 block-one-right">
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
            </div>

        </div>
    </div>
</div>
<!-- end block one -->
<!-- block two -->
<div class="container block-two">

    <div class="row">
        <div class="w-65">
            <div class="">
                <div class="row block-two-title">
                    <h2>Tin tức Liên thông</h2> <span class="block-two-link"><a href="">Các Trường Liên
                            Thông</a></span><span><a href="" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3">
                    @foreach ($tinlienthong as $item)
                    <div class=" col-xs-12 col-md-4  col-sm-12">
                    <a href="{{ route('allslug',$item->slugs->slug)}}">
                            <img src="{{ $item->image}}" class="img-fluid" alt="">
                        </a>
                        <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                        <h2>{{ $item->title}}</h2>
                        </a>
                        <p>{{$item->description}}</p>
                    </div>
                    @endforeach
                    <div class="clear"></div>
                </div>
            </div>

            <div class="">
                <div class="row block-two-title">
                    <h2>Điểm chuẩn Đại học</h2> <span class="block-two-link"><a href="">Các Trường Liên
                            Thông</a></span><span><a href="" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3">
                    @foreach ($diemchuandh as $item)
                    <div class=" col-xs-12 col-md-4  col-sm-12">
                    <a href="{{ route('allslug',$item->slugs->slug)}}">
                    <img src="{{ $item->image}}" class="img-fluid" alt="">
                        </a>
                        <a href="{{ route('allslug',$item->slugs->slug)}}" class="block-two-ct3-h2">
                            <h2>{{$item->title}}</h2>
                        </a>
                    <p>{{ $item->description}}</p>
                    </div>
                    @endforeach

                    <div class="clear"></div>
                </div>
            </div>

            <div class="">
                <div class="row block-two-title">
                    <h2>Khối thi Đại học</h2> <span class="block-two-link"><a href="">Các Trường Liên
                            Thông</a></span><span><a href="" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3">
                    <div class="col-md-6 pt-1">
                        <a href="">
                            <img src="./images/block1111.jpg" class="img-fluid" alt="">
                        </a>
                        <a href="" class="block-two-ct3-h2">
                            <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                        </a>
                        <p>Danh sách các trường Đại Học và Học Viện tuyển sinh liên thông Đại học 2020 hệ chính
                            quy, được chuyên trang thông tin tuyển sinh</p>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <ul class=" block-two-ct6-h2">
                                <li class="pt-1">
                                    <div class="col-md-5 p-0">
                                        <img src="./images/222.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </div>
                                </li>
                                <li class="pt-1">
                                    <div class="col-md-5 p-0">
                                        <img src="./images/222.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </div>
                                </li>
                                <li class="pt-1">
                                    <div class="col-md-5 p-0">
                                        <img src="./images/222.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </div>
                                </li>
                                <li class="pt-1">
                                    <div class="col-md-5 p-0">
                                        <img src="./images/222.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
        <div class="w-35">
            <div class="col-md-12 pt-md-4">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                        <h2 class="siderbar-1-title p-2">TUYỂN SINH ĐẠI HỌC</h2></span><span><a href=""
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="row m-0 ">
                        <ul class="pl-0">
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <a href="">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </a>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-md-12 pt-md-4">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                        <h2 class="siderbar-1-title p-2">TUYỂN SINH CAO ĐẲNG</h2></span><span><a href=""
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="row m-0 ">
                        <ul class="pl-0">
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-md-12 pt-md-4">
                <div class="row m-0  pt-md-3  siderbar">
                    <div class="row m-0">
                        <h2 class="siderbar-1-title p-2">TUYỂN SINH TRUNG CẤP</h2></span><span><a href=""
                                class="btn-view-all">xem toàn bộ<i class="fa fa-angle-right"
                                    aria-hidden="true"></i></a></span>
                    </div>

                    <div class="row m-0 ">
                        <ul class="pl-0">
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                            <li class="pt-1">
                                <div class="col-md-3 p-0">
                                    <img src="./images/222.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-9">
                                    <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>
<!-- end block two -->
<!-- block three -->
<div class="container block-three">
    <div class="row">
        <div class="w-72">
            <div class="">
                <div class="row block-two-title">
                    <h2>Tin giáo dục</h2><span><a href="" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0 pt-md-2">
                    <div class="col-md-3">
                        <a href="">
                            <img src="./images/block1111.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    <div class="col-md-9">
                        <a href="">
                            <h2 class="p-0 block-three-title-child">Liên Thông Trái Ngành – Các Trường Đào Tạo
                                Trái </h2>
                        </a>
                        <p>Danh sách các trường Đại Học và Học Viện tuyển sinh liên thông Đại học 2020 hệ chính
                            quy, được chuyên trang thông tin tuyển sinh</p>
                        <ul class="pl-3">
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="">
                <div class="row block-two-title">
                    <h2>Tin giáo dục</h2><span><a href="" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0  pt-md-2">
                    <div class="col-md-3">
                        <a href="">
                            <img src="./images/block1111.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    <div class="col-md-9">
                        <a href="">
                            <h2 class="p-0 block-three-title-child">Liên Thông Trái Ngành – Các Trường Đào Tạo
                                Trái </h2>
                        </a>
                        <p>Danh sách các trường Đại Học và Học Viện tuyển sinh liên thông Đại học 2020 hệ chính
                            quy, được chuyên trang thông tin tuyển sinh</p>
                        <ul class="pl-3">
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="">
                <div class="row block-two-title">
                    <h2>Tin giáo dục</h2><span><a href="" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0  pt-md-2">
                    <div class="col-md-3">
                        <a href="">
                            <img src="./images/block1111.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    <div class="col-md-9">
                        <a href="">
                            <h2 class="p-0 block-three-title-child">Liên Thông Trái Ngành – Các Trường Đào Tạo
                                Trái </h2>
                        </a>
                        <p>Danh sách các trường Đại Học và Học Viện tuyển sinh liên thông Đại học 2020 hệ chính
                            quy, được chuyên trang thông tin tuyển sinh</p>
                        <ul class="pl-3">
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                            <li><a href="">
                                    Học Kỹ Thuật Xây Dựng Công Trình Giao Thông Có Thất Nghiệp Không?</a>
                            </li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="">
                <div class="row block-two-title">
                    <h2>Khối thi Đại học</h2> <span class="block-two-link"><a href="">Các Trường Liên
                            Thông</a></span><span><a href="" class="btn-view-all">xem toàn bộ<i
                                class="fa fa-angle-right" aria-hidden="true"></i></a></span>
                </div>
                <div class="block-two-ct3 pb-0  pt-md-2">
                    <div class="col-md-6 pt-1">
                        <a href="">
                            <img src="./images/block1111.jpg" class="img-fluid" alt="">
                        </a>
                        <a href="" class="block-two-ct3-h2">
                            <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                        </a>
                        <p>Danh sách các trường Đại Học và Học Viện tuyển sinh liên thông Đại học 2020 hệ chính
                            quy, được chuyên trang thông tin tuyển sinh</p>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <ul class=" block-two-ct6-h2">
                                <li class="pt-1">
                                    <div class="col-md-5 p-0">
                                        <img src="./images/222.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </div>
                                </li>
                                <li class="pt-1">
                                    <div class="col-md-5 p-0">
                                        <img src="./images/222.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </div>
                                </li>
                                <li class="pt-1">
                                    <div class="col-md-5 p-0">
                                        <img src="./images/222.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </div>
                                </li>
                                <li class="pt-1">
                                    <div class="col-md-5 p-0">
                                        <img src="./images/222.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <h2>Liên Thông Trái Ngành – Các Trường Đào Tạo Trái </h2>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="w-28 block-three-right">
            <div class="col-md-12">
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
                <a href="">
                    <img src="./images/block1111.jpg" class="img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end block three -->
@endsection