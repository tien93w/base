@foreach($comments as $k=>$comment)


   <div class="wc-comment wc-blog-guest wc_comment_level-1">
      <div class="wc-comment-left ">
         <div class="wpd-xborder"></div>
         <img alt="{{ $comment->name }}"
            src="{{ $comment->user_id != NULL ? $comment->user->image : asset('images/gavatar.png') }}"
            style="border: 1px solid #ddd;
      border-radius: 50%;
      box-shadow: none;padding:1px" height="64" width="64">
      </div>
      <div class="wc-comment-right">
         <div class="wc-comment-header">
            <div class="wc-comment-author ">
               {{ $comment->user_id != NULL ? $comment->user->name : $comment->name }}
            </div>
            <div class="wpdiscuz_clear"></div>
         </div>
         <div class="wc-comment-text">
            <p>{{ $comment->body }}</p>
         </div>
         <div class="wc-comment-footer">
            <div class="wc-footer-left">
               <span data-toggle="collapse" data-target="#commentdemo-{{ $comment->id }}"
                  class="collapse wc-reply-button wc-cta-button" title="Trả lời"><i class="fa fa-comments"
                     aria-hidden="true"></i> Trả lời</span>

               @can('post-edit')
                  <span data-toggle="collapse" data-target="#commentedit-{{ $comment->id }}"
                     class="collapse wc-reply-button wc-cta-button" title="Trả lời"><i class="fa fa-comments"
                        aria-hidden="true"></i>Sửa Comment</span>
                  <span><a href="{{ route('delcommentfront',$comment->id) }}"
                        data-toggle="tooltip" class="collapse wc-reply-button wc-cta-button"
                        data-original-title="Delete"
                        class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill deleteUser"
                        title="XOA">
                        <i class="la la-close"></i>Xoa</a></span>
               @endcan
            </div>
            @if(count($comment->replies) >0)
               <div class="wc-footer-right">
                  <div class="wc-toggle pull-right" style="" aria-expanded="false" data-toggle="collapse"
                     data-target="#commentde-{{ $comment->id }}" "><i class=" fa fa-chevron-down"
                     title="Hiện trả lời"></i></div>
               </div>
            @endif

            <div class="wpdiscuz_clear"></div>
         </div>
      </div>
      @can('post-edit')
         <div class="collapse wc-form-wrapper wc-secondary-form-wrapper" id="commentedit-{{ $comment->id }}" style="">
            <div class="wpdiscuz-comment-message" style="display: block;"></div>
            <div class="wc-secondary-forms-social-content"></div>
            <div class="clearfix"></div>

            <form class="wc_comm_form wc-secondary-form-wrapper" method="post" enctype="multipart/form-data"
               action="{{ route('comment.update',$comment->id) }}">
               @csrf
               {{ method_field('PATCH') }}
               <div class="wc-field-comment">
                  <div class="wpdiscuz-item wc-field-textarea">
                     <div class="wpdiscuz-textarea-wrap ">
                        <div class="wc-field-avatararea">
                           <img alt="avatar" src="https://secure.gravatar.com/avatar/?s=48&amp;d=mm&amp;r=g"
                              srcset="https://secure.gravatar.com/avatar/?s=96&amp;d=mm&amp;r=g 2x"
                              class="avatar avatar-48 photo avatar-default" height="48" width="48">
                        </div>
                        <textarea placeholder="Tham gia thảo luận..." required="" name="body"
                           class="wc_comment wpd-field"
                           style="overflow: hidden; min-height: 2em; height: 60px;">{{ $comment->body }}</textarea>

                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="wc-form-footer" style="display: block;">
                  <div class="wpd-form-row">
                     <div class="wpd-form-col-left" style="">
                        <div class="wpdiscuz-item wc_name-wrapper wpd-has-icon">
                           <div class="wpd-field-icon"><i class="fa fa-user"></i></div>
                           <input value="{{ $comment->name }}" required="required" class="wc_name wpd-field"
                              type="text" name="name" placeholder="Họ Và Tên*" maxlength="50" pattern=".{3,50}"
                              title="">
                           <div class="wpd-field-desc"><i class="fa fa-question-circle"
                                 aria-hidden="true"></i><span>Nhập họ tên đầy đủ</span></div>
                        </div>
                     </div>
                     <div class="wpd-form-col-right">
                        <div class="wc-field-submit">
                           <input class="wc_comm_submit wc_not_clicked button alt" type="submit" name="submit"
                              value="Sửa bình luận">
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </form>
         </div>
         <div class="wpdiscuz_clear"></div>
      @endcan
      <div class="wpdiscuz-comment-message"></div>
      <div class="collapse wc-form-wrapper wc-secondary-form-wrapper" id="commentdemo-{{ $comment->id }}" style="">
         <div class="wpdiscuz-comment-message" style="display: block;"></div>
         <div class="wc-secondary-forms-social-content"></div>
         <div class="clearfix"></div>

         <form class="wc_comm_form wc-secondary-form-wrapper" method="post" enctype="multipart/form-data"
            action="{{ route('reply.add') }}">
            @csrf
            <input type="hidden" name="post_id" value="{{ $comment->post_id }}">
            <input type="hidden" name="comment_id" value="{{ $comment->id }}">
            <div class="wc-field-comment">
               <div class="wpdiscuz-item wc-field-textarea">
                  <div class="wpdiscuz-textarea-wrap ">
                     <div class="wc-field-avatararea">
                        <img alt="avatar" src="https://secure.gravatar.com/avatar/?s=48&amp;d=mm&amp;r=g"
                           srcset="https://secure.gravatar.com/avatar/?s=96&amp;d=mm&amp;r=g 2x"
                           class="avatar avatar-48 photo avatar-default" height="48" width="48">
                     </div>
                     <textarea placeholder="Tham gia thảo luận..." required="" name="body" class="wc_comment wpd-field"
                        style="overflow: hidden; min-height: 2em; height: 60px;"></textarea>
                     <div class="autogrow-textarea-mirror"
                        style="display: none; overflow-wrap: break-word; padding: 10px; width: 495px; font-family: &quot;Helvetica Neue&quot;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px;">
                        .<br>.</div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="wc-form-footer" style="display: block;">
               <div class="wpd-form-row">
                  <div class="wpd-form-col-left"
                     style="{{ Auth::check() ? 'display:none':"" }}">
                     <div class="wpdiscuz-item wc_name-wrapper wpd-has-icon">
                        <div class="wpd-field-icon"><i class="fa fa-user"></i></div>
                        <input
                           value="{{ Auth::check() ? Auth::user()->name: old('name') }}"
                           required="required" class="wc_name wpd-field"
                           type="{{ Auth::check() ? 'hidden':'text' }}"
                           name="name" placeholder="Họ Và Tên*" maxlength="50" pattern=".{3,50}" title="">
                        <div class="wpd-field-desc"><i class="fa fa-question-circle" aria-hidden="true"></i><span>Nhập
                              họ tên đầy đủ</span></div>
                     </div>
                     <div class="wpdiscuz-item custom_field_5ea2bc76c8f3f-wrapper wpd-has-icon wpd-has-desc">
                        <div class="wpd-field-icon"><i style="opacity: 0.8;" class="fa fa-phone"></i></div>
                        <input required="required" class="custom_field_5ea2bc76c8f3f wpd-field wpd-field-number"
                           type="{{ Auth::check() ? 'hidden':'text' }}"
                           name="phone"
                           value="{{ Auth::check() ? "0999384382" :old('phone') }}"
                           placeholder="Điện Thoại*">
                        <div class="wpd-field-desc"><i class="fa fa-question-circle" aria-hidden="true"></i><span>Điện
                              thoại liên hệ</span></div>
                     </div>
                     <div class="wpdiscuz-item wc_email-wrapper wpd-has-icon">
                        <div class="wpd-field-icon"><i class="fa fa-at"></i></div>
                        <input required="required" class="wc_email wpd-field" type="email"
                           value="{{ Auth::check() ? Auth::user()->email:old('email') }}"
                           name="email" placeholder="Địa chỉ Email*">
                        <div class="wpd-field-desc"><i class="fa fa-question-circle" aria-hidden="true"></i><span>Thư
                              điện tử</span></div>
                     </div>
                  </div>
                  <div class="wpd-form-col-right">
                     <div class="wc-field-submit">
                        <input class="wc_comm_submit wc_not_clicked button alt" type="submit" name="submit"
                           value="Gửi bình luận">
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
            <div class="clearfix"></div>
         </form>
      </div>
      <div class="wpdiscuz_clear"></div>

   </div>
   <div id="commentde-{{ $comment->id }}"
      class="collapse {{ $k !=0 ? "":"in" }} wc-comment wc-reply wc-blog-user wc-blog-administrator wc_comment_level-2"
      style="" aria-expanded="">
      @include('partials._comment_replies', ['comments' => $comment->replies])
   </div>
@endforeach