<?php ?>
@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Thông tin học viên</div>

                <div class="panel-body">


                    <div class="form-group">
                        <label for="title" class="col-md-4 control-label">Tên học viên</label>
                        {{ $data->name }}
                    </div>


                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Ngày sinh</label>
                        {{ $data->date }}
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Giới tính</label>
                        {{ $data->sex }}
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Số điện thoại</label>
                        {{ $data->phone }}
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Địa chỉ</label>
                        {{ $data->address }}
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Tên phụ huynh</label>
                        {{ $data->nameparent }}
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Điểm</label>
                        {{ $data->point }}
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Số điện thoại phụ huynh</label>
                        {{ $data->phoneparent }}
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Hệ xét tuyển</label>
                        {{ $data->admission }}
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Facebook</label>
                        <a href="{{ $data->facebook }}">{{ $data->facebook }}</a>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection