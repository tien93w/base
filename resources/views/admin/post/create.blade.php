<?php ?>

@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Tạo bài viết
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>

        </div>
        <div class="m-portlet__body">

            @include('admin.errors')
            <form class="form-horizontal" style="width:100%" role="form" method="POST"
                action="{{ route('post.store') }}">

                <div class="row">

                    {{ csrf_field() }}
                    <div class="col-md-9">


                        <div
                            class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-12 control-label">Tên bài viết</label>

                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title"
                                    value="{{ old('title') }}" onkeyup="ChangeToSlug()" required
                                    autofocus>

                                @if($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div
                            class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                            <label for="slug" class="col-md-12 control-label">Slug bài viết</label>

                            <div class="col-md-12">
                                <input id="slug" type="text" class="form-control" name="slug"
                                    value="{{ old('slug') }}">

                                @if($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="">Danh muc</label>
                            <select name="cid" class="form-control">
                                {{ old('cid') }}
                                <option value="">Chọn</option>
                                <?php categoryParent($data,$parent = 0, $str="", $select =old('cid'))?>
                            </select>
                        </div>

                        <div
                            class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-12 control-label">Mô tả ngắn</label>

                            <div class="col-md-12">
                                <textarea rows="7" id="description" name="description"
                                    class="form-control my-editor">{{ old('description') }}</textarea>
                                @if($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div
                            class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-12 control-label">Nội dung</label>

                            <div class="col-md-12">
                                <textarea id="content" rows="40" name="content"
                                    class="form-control my-editor">{{ old('content') }}</textarea>
                                @if($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('tag', 'Tags', ['class' => 'control-label']) !!}
                                {!! Form::select('tag[]', $tags, old('tag'), ['class' => 'form-control select2',
                                'multiple' => 'multiple', 'id' => 'selectall-tag' ]) !!}
                                <p class="help-block"></p>
                                @if($errors->has('tag'))
                                    <p class="help-block">
                                        {{ $errors->first('tag') }}
                                    </p>
                                @endif
                                <div style="color: #0E0EFF;padding-bottom: 30px">
                                    <a style="background-color: #fdf7f7" type="button" class="btn  btn-xs"
                                        id="selectbtn-tag">
                                        Chọn tất cả tag
                                    </a>
                                    <a style="background-color: #fdf7f7" type="button" class="btn btn-xs"
                                        id="deselectbtn-tag">
                                        Xoá tất cả tag
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 link-postcustom" style="display: none">
                            <h2>Linh liên kết</h2>
                            <div class="form-group">
                                <label for="">Linh liên kết 1</label>
                                <input type="text" class="form-control" name="link1" placeholder="Nhap link">
                            </div>
                            <div class="form-group">
                                <label for="">Linh liên kết 2</label>
                                <input type="text" class="form-control" name="link2" placeholder="Nhap link">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2>SEO
                                <span style="font-size: 40px; float: right" class="" data-toggle="collapse"
                                    data-target="#demo"><i class="fa fa-angle-double-down"
                                        style="font-size: 40px;color: #0b51c5" aria-hidden="true"></i></span>
                            </h2>
                        </div>
                        <div id="demo" class="collapse">


                            <div
                                class="form-group{{ $errors->has('meta_title') ? ' has-error' : '' }}">
                                <label for="meta_title" class="col-md-12 control-label">Meta Title</label>

                                <div class="col-md-12">
                                    <div id="ameta_title"></div>

                                    <input rows="7" id="meta_title" name="meta_title" class="form-control"
                                        value="{{ old('meta_title') }}"></input>
                                    @if($errors->has('meta_title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('meta_title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div
                                class="form-group{{ $errors->has('keywords') ? ' has-error' : '' }}">
                                <label for="keywords" class="col-md-12 control-label">Meta Keywords</label>

                                <div class="col-md-12">
                                    <div id="akeywords"></div>
                                    <input rows="7" id="keywords" name="keywords" class="form-control"
                                        value="{{ old('keywords') }}"/>
                                    @if($errors->has('keywords'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('keywords') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div
                                class="form-group{{ $errors->has('mdescription') ? ' has-error' : '' }}">
                                <label for="mdescription" class="col-md-12 control-label">Meta Description</label>

                                <div class="col-md-12">
                                    <div id="amdescription"></div>
                                    <textarea rows="4" id="mdescription" name="mdescription"
                                        class="form-control">{{ old('mdescription') }}</textarea>
                                    @if($errors->has('mdescription'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mdescription') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-12" style="padding-top: 30px">
                                <button type="submit" class="btn btn-primary">
                                    Tạo mới
                                </button>

                                <a class="btn btn-link" href="{{ route('post.index') }}">
                                    Hủy
                                </a>
                            </div>
                        </div>


                    </div>
                    {{-- end md9 --}}
                    <div class="col-md-3">
                        <div class="form-group row" style="margin:0px">
                            {{-- <div class="col-md-12">
                                        <div class="m-form__group form-group row">
                                            <label class="col-3 col-form-label">Bài viết nổi bật</label>
                                            <div class="col-3">
                                                <span class="m-switch m-switch--success">
                                                    <label>
                                                    <input type="checkbox" checked="checked" name="feature">
                                                    <span></span>
                                                    </label>
                                                </span>
                                            </div>
                                            <label class="col-3 col-form-label">Public bài viết</label>
                                            <div class="col-3">
                                                <span class="m-switch m-switch--warning">
                                                    <label>
                                                    <input type="checkbox" checked="checked" name="public">
                                                    <span></span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                    </div> --}}
                            <div class="col-md-12">
                                <label for="image" class="control-label">Ảnh đại diện</label><br>
                                <span class="form-group-btn">
                                    <a id="lfm" data-input="image" data-preview="holder"
                                        class="btn btn-primary text-white">
                                        <i class="fa fa-picture-o"></i> Chọn
                                    </a>
                                </span>
                                <input id="image" class="form-control" type="text" name="image"
                                    value="{{ old('image') }}">
                                <div id="holder" style="margin-top:15px;max-height:300px;"></div>
                            </div>

                            <div class="col-md-12">
                                <h3>Post template</h3>
                                <select name="type" id="type" onchange="pageType(this)" class="form-control">
                                    <option
                                        {{ old('page-type') == 'new' ? 'selected' :'' }}
                                        value="new">Default</option>
                                    <option
                                        {{ old('page-type') == 'landing' ? 'selected' :'' }}
                                        value="landing">Landing</option>
                                    <option
                                        {{ old('page-type') == 'newcustom' ? 'selected' :'' }}
                                        value="newcustom">Custom Post</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('/vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
{{-- <script src="{{ asset('js/tinymce.min.js') }}"></script> --}}
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
    function pageType(obj) {
        var value = obj.value;
        if (value === 'newcustom') {
            $('.link-postcustom').css({
                'display': 'block'
            })
        }
        if (value === 'new') {
            $('.link-postcustom').css({
                'display': 'none'
            })
        }
    }
    $('#lfm').filemanager('image');

    function ChangeToSlug() {
        var title, slug;

        //Lấy text từ thẻ input title
        title = document.getElementById("title").value;

        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();

        //Đổi ký tự có dấu thành không dấu
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, "-");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”
        // var url = '{{ url('/page/') }}';
        // document.getElementById('link').value = url +'/'+ slug;
        document.getElementById('slug').value = slug;

    }
    var editor_config = {
        path_absolute: "/",
        selector: "textarea.my-editor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "forecolor backcolor insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | code",
        relative_urls: false,
        image_caption: true,
        image_title: true,
        style_formats: [{
                title: "Image",
                items: [{
                        title: "Left",
                        icon: "alignleft",
                        selector: "img,figure",
                        classes: "image align-left"
                    },
                    {
                        title: "Center",
                        icon: "aligncenter",
                        selector: "img,figure",
                        classes: "image align-center"
                    },
                    {
                        title: "Right",
                        icon: "alignright",
                        selector: "img,figure",
                        classes: "image align-right"
                    },
                    {
                        title: "Full",
                        icon: "alignjustify",
                        selector: "img,figure",
                        classes: "image featured"
                    },

                ]
            },
            {
                title: 'Headers',
                items: [{
                        title: 'h1',
                        block: 'h1'
                    },
                    {
                        title: 'h2',
                        block: 'h2'
                    },
                    {
                        title: 'h3',
                        block: 'h3'
                    },
                    {
                        title: 'h4',
                        block: 'h4'
                    },
                    {
                        title: 'h5',
                        block: 'h5'
                    },
                    {
                        title: 'h6',
                        block: 'h6'
                    },
                    {
                        title: 'normal',
                        block: 'p'
                    }
                ]
            },
        ],
        style_formats_merge: false,
        menubar: true,
        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName(
                'body')[0].clientWidth;
            var y = window.innerHeight || document.documentElement.clientHeight || document
                .getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'Filemanager',
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",

                close_previous: "no"
            });
        }
    };

    tinymce.init(editor_config);
</script>
<script>
    $("#selectbtn-tag").click(function () {
        $("#selectall-tag > option").prop("selected", "selected");
        $("#selectall-tag").trigger("change");
    });
    $("#deselectbtn-tag").click(function () {
        $("#selectall-tag > option").prop("selected", "");
        $("#selectall-tag").trigger("change");
    });

    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
<script>
    $('#meta_title').keyup(function () {
        $('#ameta_title').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
    $('#keywords').keyup(function () {
        $('#akeywords').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
    $('#mdescription').keyup(function () {
        $('#amdescription').text('Bạn đã nhập ' + this.value.length + ' ký tự');
    });
</script>
</div>


@endsection