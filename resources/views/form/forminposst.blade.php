<div id="forminpost">
    <h2 class="text-center">ĐĂNG KÝ XÉT TUYỂN TRỰC TUYẾN</h2>
  <form class="form-horizontal"  action="{{ route('reg') }}" method="post">
      {{ csrf_field() }}
    <div class="form-group">
        <label class="hidden-xs control-label col-sm-3" for="student">Họ và tên:<span>*</span></label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="student" placeholder="Vui lòng nhập đầy đủ họ và tên" name="student">
        </div>
      </div>
    <div class="form-group">
      <label class="hidden-xs control-label col-sm-3" for="email">Ngày tháng năm sinh:<span>*</span></label>
      <div class="col-sm-9">
        <input type="date" class="form-control" id="email" placeholder="" name="date">
      </div>
    </div>
    <div class="form-group">
        <label class="hidden-xs control-label col-sm-3" for="phone">Số điện thoại:<span>*</span></label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="phone" placeholder="Vui lòng nhập số điện thoại" name="phone">
        </div>
    </div>
    <div class="form-group">
        <label class="hidden-xs control-label col-sm-3" for="address">Địa chỉ thường trú:<span>*</span></label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="address" placeholder="Vui lòng nhập địa chỉ thường trú" name="address">
        </div>
    </div>
    <div class="form-group">
        <label class="hidden-xs control-label col-sm-3" for="parent_name">Tên Phụ Huynh:<span>*</span></label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="parent_name" placeholder="Vui lòng nhập tên bố hoặc mẹ" name="parent_name">
        </div>
    </div>
    <div class="form-group">
        <label class="hidden-xs control-label col-sm-3" for="parent_phone">Số Điện Thoại Phụ Huynh:<span>*</span></label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="parent_phone" placeholder="Vui lòng nhập số điện thoại bố hoặc mẹ" name="parent_phone">
        </div>
    </div>
    <div class="form-group">
        <label class="hidden-xs control-label col-sm-3" for="addmission">Chọn Hệ Đăng Ký:<span>*</span></label>
        <div class="col-sm-9">
          <select name="addmission" id="addmission" class="form-control">
              <option value="Hệ cao đẳng nấu ăn">Cao Đẳng Nấu Ăn</option>
              <option value="Hệ trung cấp nấu ăn">Trung Cấp Nấu Ăn</option>
              <option value="Sơ cấp nấu ăn">Sơ Cấp Nấu Ăn</option>
          </select>
        </div>
    </div>
    <div class="form-group">
        <label class="hidden-xs control-label col-sm-3" for="facebook">Địa Chỉ Facebook cá nhân:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="facebook" placeholder="Vui lòng nhập link liên kết facebook cá nhân" name="facebook">
        </div>
    </div>
    <div class="form-group contact-form">        
      <div class="col-sm-12">
        <button type="submit" class="btn btn-primary center-block btn-dangky-home">
            GỬI THÔNG TIN
        </button>
      </div>
    </div>
  </form>
</div>
